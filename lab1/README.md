#Lab 1: Working with Mono and Flux

This lab have exercies which will make you understand how to use Mono and Flux

## Exercises
* Simple create https://medium.com/@cheron.antoine/reactor-java-1-how-to-create-mono-and-flux-471c505fa158
* Call other function if empty
* When to use the ~~.block()~~ stepVerifier()
* Fault handling

## Next Exercises
* Backpressure 
* Functional Endpoints
* WebClient
* Transforming data  https://medium.com/@cheron.antoine/reactor-java-2-how-to-manipulate-the-data-inside-mono-and-flux-b36ae383b499

# Resources
https://www.baeldung.com/reactor-core
https://medium.com/@cheron.antoine/reactor-java-1-how-to-create-mono-and-flux-471c505fa158
https://docs.spring.io/spring/docs/current/spring-framework-reference/web-reactive.html#webflux-new-framework
https://www.codingame.com/playgrounds/929/reactive-programming-with-reactor-3/transform


