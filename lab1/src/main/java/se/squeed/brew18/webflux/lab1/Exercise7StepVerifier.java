package se.squeed.brew18.webflux.lab1;

import java.time.Duration;

import reactor.core.publisher.Flux;

public class Exercise7StepVerifier {


    /**
     * @return a flux of values 1..5
     */
    public Flux<Integer> getFluxCounter() {
        return Flux.range(1, 4);
    }


    /**
     * @return Flux which return 0 after 10 seconds, then 1 after another 1 second
     */
    public Flux<Long> getIntervalFlux() {
        return Flux.interval(Duration.ofSeconds(10), Duration.ofSeconds(5)).take(2);
    }


}
