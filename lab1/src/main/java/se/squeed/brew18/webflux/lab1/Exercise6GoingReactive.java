package se.squeed.brew18.webflux.lab1;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.util.Arrays;
import java.util.List;

public class Exercise6GoingReactive {
    private BlockingRepository blockingRepository = new BlockingRepository();

    /**
     * Make a call to the Blockingrepository.getListOfIntegers and convert it to flux, but have it running on a
     * different thread
     * Hint: Schedulers.elastic()
     *
     * @return return a list of integers
     */
    public Flux<Integer> getFluxFromBlockingAPI() {
        return null;
    }


    private class BlockingRepository {
        public List<Integer> getListOfIntegers() {
            return Arrays.asList(1, 2, 3, 4);
        }
    }


}
