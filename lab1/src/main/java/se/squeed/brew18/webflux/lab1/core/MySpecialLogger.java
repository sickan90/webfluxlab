package se.squeed.brew18.webflux.lab1.core;

public class MySpecialLogger {

    /**
     * Very very special logger
     * @param message to log
     */
    public void info(String message) {
        System.out.println(message);
    }
}
