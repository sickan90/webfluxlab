package se.squeed.brew18.webflux.lab1;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class Exercise3Transformations {

    /**
     * Use .map() to return a mono which is capitalized. Note that this is synchronous.
     * @param mono
     * @return mono which is capitalized using .map()
     */
    Mono<String> capitalizeMono(Mono<String> mono) {
        return null;
    }

    /**
     * Use .map() to return a flux which is capitalized. Note that this is synchronous.
     * @param flux of users
     * @return flux which is capitalized using .map()
     */
    Flux<String> capitalizeFlux(Flux<String> flux) {
        return null;
    }

    /**
     * Use .flatMap() to return a flux which is capitalized. Note that this is asynchronous.
     * @param flux of users
     * @return flux which is capitalized using .flatMap()
     */
    Flux<String> asyncCapitalizeFlux(Flux<String> flux) {
        return null;
    }

}
