package se.squeed.brew18.webflux.lab1.core;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;

public class UserService {

    public static String getAUser() {
        return "Mario";
    }

    public static CompletableFuture<String> getFutureUser() {
        CompletableFuture<String> completableFuture = new CompletableFuture<>();

        Executors.newCachedThreadPool().submit(() -> {
            Thread.sleep(200);
            completableFuture.complete("Mario");
            return null;
        });

        return completableFuture;
    }

}
