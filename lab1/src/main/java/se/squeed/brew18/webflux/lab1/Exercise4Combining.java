package se.squeed.brew18.webflux.lab1;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Good tutorial: https://www.baeldung.com/reactor-combine-streams
 */
public class Exercise4Combining {


    /**
     * Use the flux's concat method to combine two flux.
     *
     * @return a concat flux list of both flux1 and flux2
     */
    Flux<Integer> combineWithConcat(Flux<Integer> flux1, Flux<Integer> flux2) {
        return null;
    }

    /**
     * Use the flux's merge method to combine two flux. Does this asynchronous
     *
     * @return a merged flux list of both flux1 and flux2
     */
    Flux<Integer> combineWithMerge(Flux<Integer> flux1, Flux<Integer> flux2) {
        return null;
    }


    /**
     * Use the flux's zip method to sum two flux. ex: {1, 3} and {2, 4} will return {3, 7}
     *
     * @return a list of the sum of a odd and a even number
     */
    Flux<Integer> combineWithZip(Flux<Integer> flux1, Flux<Integer> flux2) {
        return null;
    }

    /**
     * Hint: concat
     *
     * @return a flux combining mono1 and mono2
     */
    Flux<Integer> createFluxFromMultipleMono(Mono<Integer> mono1, Mono<Integer> mono2) {
        return null;
    }

}
