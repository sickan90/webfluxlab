package se.squeed.brew18.webflux.lab1;

import java.util.ArrayList;
import java.util.List;

import reactor.core.publisher.Flux;
import se.squeed.brew18.webflux.lab1.core.MySpecialLogger;

/**
 * In this exercise we are going to look a couple of common ways to create Flux objects
 */
public class Exercise2Subscribe {

    MySpecialLogger logger = new MySpecialLogger();


    /**
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Flux.html#subscribe-java.util.function.Consumer-
     *
     * @param flux of integers
     */
    public List<Integer> toArrayListThroughSubscribe(Flux<Integer> flux) {
        List<Integer> elements = new ArrayList<>();
        //TODO: impl
        return elements;
    }

    /**
     * Print the error message instead of failing in the logger.info()
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Flux.html#subscribe-java.util.function.Consumer-
     *
     * @param flux which will cast an error
     */
    public void printErrorMessage(Flux<Integer> flux) {
        //TODO: impl
    }

    /**
     * Print the completeMessage on complete in logger.info()
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Flux.html#subscribe-java.util.function.Consumer-
     *
     * @param flux which will finnish winout any problems
     * @param completeMessage containing the message to print when completed
     */
    public void printOnComplete(Flux<Integer> flux, String completeMessage) {
        //TODO: Impl
    }


}

