package se.squeed.brew18.webflux.lab1;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.squeed.brew18.webflux.lab1.core.UserService;


/**
 * In this exercice we will be looking at creating Mono objects in different ways
 */
public class Exercise1MonoAndFlux {


    /**
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Mono.html#just-T-
     *
     * @return a string "Mario" wrapped in a Mono
     */
    public Mono<String> getName() {
        return Mono.just("Mario");
    }

    /**
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Mono.html#empty--
     *
     * @return an empty Mono object
     */
    public Mono<String> getEmpty() {
        return null;
    }

    /**
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Mono.html#fromCallable-java.util.concurrent.Callable-
     *
     * @return a string from the UserService.getAUser() wrapped in a Mono
     */
    public Mono<String> getFromFunciton() {
        return null;
    }

    /**
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Mono.html#fromFuture-java.util.concurrent.CompletableFuture-
     *
     * @return a string from the UserService.getFutureUser() wrapped in a Mono
     */
    public Mono<String> getFromFuture() {
        return null;
    }


    /**
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Mono.html#fromSupplier-java.util.function.Supplier-
     *
     * @return get an Integer from a supplier such as the java.util.Random wrapped in a Mono
     */
    public Mono<Integer> getFromSupplier() {
        Random random = new Random();
        return null;
    }

    /**
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Mono.html#from-org.reactivestreams.Publisher-
     *
     * @return create another mono from the getName() mono
     */
    public Mono<String> getFromMono() {
        return null;
    }

    /**
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Mono.html#create-java.util.function.Consumer-
     *
     * @return a mono which return "Mario" using Mono.create
     */
    public Mono<String> getFromCreate() {
        return null;
    }


    // ---------------------------------------------------------------------------------------
    // -- Flux -------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------

    /**
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Flux.html#just-T...-
     *
     * @return a flux with values "Hello" and "World" from using _just_
     */
    public Flux<String> getSentenceFromJust() {
        return null;
    }

    /**
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Flux.html#fromIterable-java.lang.Iterable-
     *
     * @return a flux with values "Hello" and "World" from using _iterable_
     */
    public Flux<String> getSentenceFromList() {
        List<String> list = Arrays.asList("Hello", "World");
        return null;
    }

    /**
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Flux.html#fromStream-java.util.stream.Stream-
     *
     * @return a flux with values "Hello" and "World" from using _stream_
     */
    public Flux<String> getSentenceFromStream() {
        Stream<String> stream = Arrays.asList("Hello", "World").stream();
        return null;
    }

    /**
     * Create a flux which is "triggered" after 10 seconds and then after each 5th second.
     * Return the first 3 values only. (Hint: _interval_ and _take_)
     *
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Flux.html#interval-java.time.Duration-java.time.Duration-
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Flux.html#take-long-
     *
     * @return a flux which is triggered 3 time every 5 seconds
     */
    public Flux<Long> getInterval3Times() {
        return null;
    }

    /**
     * @return a flux with values "Hello" and "World" from using another function such as #getSentenceFromJust()
     */
    public Flux<String> getFromOtherFlux() {
        return null;
    }

    /**
     * https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Flux.html#create-java.util.function.Consumer-
     *
     * @return a flux which has int values 0..4 and then completes
     */
    public Flux<Integer> getFromCreateFlux() {
        return null;
    }

}
