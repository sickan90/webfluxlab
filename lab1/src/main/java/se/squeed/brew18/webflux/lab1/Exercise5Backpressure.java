package se.squeed.brew18.webflux.lab1;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;


public class Exercise5Backpressure {

    /**
     * In this exersice we are going to subscribe to a flux but control how much data we want to process and how fast.
     * The task is to fill the todos.
     *
     * The log output from the test should be something like:
     *     request(2)
     *     onNext(1)
     *     onNext(2)
     *     request(2)
     *     onNext(3)
     *     onNext(4)
     *     request(2)
     *     onComplete()
     *
     */
    public void getBackpressured(Flux<Integer> flux) {
        MySubscriber mySubscriber = new MySubscriber();
        flux.subscribe(mySubscriber);
    }

    private class MySubscriber implements Subscriber<Integer> {
        Subscription subscription;
        int counter = 0;

        @Override
        public void onSubscribe(Subscription subscription) {
            this.subscription = subscription;

            // TODO: Tell that you only want to fetch 2 items
        }

        @Override
        public void onNext(Integer integer) {
            counter++;
            if (counter % 2 == 0) {
                // TODO: Now we want to fetch another two items
            }
        }

        @Override
        public void onError(Throwable throwable) { }

        @Override
        public void onComplete() { }

    }

}
