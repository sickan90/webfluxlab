package se.squeed.brew18.webflux.lab1;

import java.time.Duration;
import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import reactor.test.scheduler.VirtualTimeScheduler;

public class Exercise1MonoAndFluxTest {

    private Exercise1MonoAndFlux exercise;

    @BeforeEach
    public void setup() {
        exercise = new Exercise1MonoAndFlux();
    }

    @Test
    void getName() {
        Mono<String> name = exercise.getName();
        StepVerifier.create(name)
                .expectNext("Mario")
                .verifyComplete();
    }

    @Test
    void getEmpty() {
        Mono<String> name = exercise.getEmpty();
        StepVerifier.create(name)
                .verifyComplete();
    }

    @Test
    void getFromFunction() {
        Mono<String> user = exercise.getFromFunciton();
        StepVerifier.create(user)
                .expectNextMatches(u -> u.equals("Mario"))
                .verifyComplete();
    }

    @Test
    void getFromFuture() {
        Mono<String> user = exercise.getFromFuture();
        StepVerifier.create(user)
                .expectNextMatches(u -> u.equals("Mario"))
                .verifyComplete();
    }

    @Test
    void getFromSupplier() {
        Mono<Integer> random = exercise.getFromSupplier();

        StepVerifier.create(random)
                .expectNextMatches(Objects::nonNull)
                .verifyComplete();
    }

    @Test
    void getFromMono() {
        Mono<String> name = exercise.getFromMono();
        StepVerifier.create(name)
                .expectNext("Mario")
                .verifyComplete();
    }

    @Test
    void getCreate() {
        Mono<String> name = exercise.getFromCreate();
        StepVerifier.create(name)
                .expectNext("Mario")
                .verifyComplete();
    }

    @Test
    void getSentence() {
        Flux<String> flux = exercise.getSentenceFromJust();
        StepVerifier.create(flux)
                .expectNext("Hello", "World")
                .verifyComplete();
    }

    @Test
    void getSentenceFromList() {
        Flux<String> flux = exercise.getSentenceFromList();
        StepVerifier.create(flux)
                .expectNext("Hello", "World")
                .verifyComplete();
    }

    @Test
    void getSentenceFromStream() {
        Flux<String> flux = exercise.getSentenceFromStream();
        StepVerifier.create(flux)
                .expectNext("Hello", "World")
                .verifyComplete();
    }

    @Test
    void getInterval3Times() {
        VirtualTimeScheduler.getOrSet();
        Flux<Long> flux = exercise.getInterval3Times();

        StepVerifier.withVirtualTime(() -> flux)
                .expectSubscription()
                .expectNoEvent(Duration.ofSeconds(10))
                .expectNext(0L)
                .expectNoEvent(Duration.ofSeconds(5))
                .expectNext(1L)
                .expectNoEvent(Duration.ofSeconds(5))
                .expectNext(2L)
                .verifyComplete();
    }

    @Test
    void getFromOtherFlux() {
        Flux<String> flux = exercise.getFromOtherFlux();
        StepVerifier.create(flux)
                .expectNext("Hello", "World")
                .verifyComplete();
    }

    @Test
    void getFromCreateFlux() {
        Flux<Integer> flux = exercise.getFromCreateFlux();
        StepVerifier.create(flux)
                .expectNext(0,1,2,3,4)
                .verifyComplete();
    }

}