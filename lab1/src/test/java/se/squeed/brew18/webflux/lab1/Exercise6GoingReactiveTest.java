package se.squeed.brew18.webflux.lab1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.*;

class Exercise6GoingReactiveTest {
    Exercise6GoingReactive exercise;

    @BeforeEach
    void setUp() {
        exercise = new Exercise6GoingReactive();
    }

    @Test
    void getFluxFromBlockingAPI() {
        Flux<Integer> flux = exercise.getFluxFromBlockingAPI().log();
        StepVerifier.create(flux)
                .expectNext(1, 2, 3, 4)
                .verifyComplete();
    }
}