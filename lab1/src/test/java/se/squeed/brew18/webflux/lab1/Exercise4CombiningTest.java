package se.squeed.brew18.webflux.lab1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;

class Exercise4CombiningTest {
    private Exercise4Combining exercise;

    private Flux<Integer> evenNumbers;
    private Flux<Integer> oddNumbers;

    private int min = 1;
    private int max = 4;

    @BeforeEach
    void setUp() {
        exercise = new Exercise4Combining();
        evenNumbers = Flux.range(min, max).filter(x -> x % 2 == 0);
        oddNumbers = Flux.range(min, max).filter(x -> x % 2 > 0);
    }

    @Test
    void testCombineWitCombine() {
        // Delay oddNumbers to be able to test that they use concat
        oddNumbers = oddNumbers.delayElements(Duration.ofMillis(50));

        Flux<Integer> numbers = exercise.combineWithConcat(oddNumbers, evenNumbers).log();
        StepVerifier.create(numbers)
                .expectNext(1,3, 2, 4)
                .verifyComplete();
    }

    @Test
    void testCombineWithMerge() {
        // Delay oddNumbers to be able to predict the merge
        oddNumbers = oddNumbers.delayElements(Duration.ofMillis(50));

        Flux<Integer> numbers = exercise.combineWithMerge(oddNumbers, evenNumbers).log();
        StepVerifier.create(numbers)
                .expectNext(2,4, 1, 3)
                .verifyComplete();
    }

    @Test
    void testCombineWithZip() {
        Flux<Integer> numbers = exercise.combineWithZip(oddNumbers, evenNumbers).log();
        StepVerifier.create(numbers)
                .expectNext(3,7)
                .verifyComplete();
    }

    @Test
    void testCreateFluxFromMultipleMono() {
        Flux<Integer> numbers = exercise.createFluxFromMultipleMono(Mono.just(1), Mono.just(2)).log();
        StepVerifier.create(numbers)
                .expectNext(1,2)
                .verifyComplete();
    }







}