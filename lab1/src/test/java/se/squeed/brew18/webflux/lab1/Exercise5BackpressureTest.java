package se.squeed.brew18.webflux.lab1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;

import static java.lang.Thread.sleep;
import static org.junit.jupiter.api.Assertions.*;

class Exercise5BackpressureTest {
    Exercise5Backpressure exercise;

    @BeforeEach
    void setUp() {
        exercise = new Exercise5Backpressure();
    }

    @Test
    void getBackpressured() {
        exercise.getBackpressured(Flux.just(1, 2, 3, 4).log());
    }

}