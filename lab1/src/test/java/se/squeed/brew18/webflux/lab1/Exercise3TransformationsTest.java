package se.squeed.brew18.webflux.lab1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

class Exercise3TransformationsTest {

    Exercise3Transformations exercise;

    @BeforeEach
    void setup() {
        exercise = new Exercise3Transformations();
    }

    @Test
    void capitalizeMono() {
        Mono<String> user = exercise.capitalizeMono(Mono.just("Mario"));
        StepVerifier.create(user)
                .expectNextMatches(u -> u.equals("MARIO"))
                .verifyComplete();
    }

    @Test
    void capitalizeFlux() {
        Flux<String> users = exercise.capitalizeFlux(Flux.just("Mario", "Luigi"));
        StepVerifier.create(users)
                .expectNext("MARIO", "LUIGI")
                .verifyComplete();
    }

    @Test
    void asyncCapitalizeFlux() {
        Flux<String> user = exercise.asyncCapitalizeFlux(Flux.just("Mario", "Luigi"));
        StepVerifier.create(user)
                .expectNext("MARIO", "LUIGI")
                .verifyComplete();
    }
}