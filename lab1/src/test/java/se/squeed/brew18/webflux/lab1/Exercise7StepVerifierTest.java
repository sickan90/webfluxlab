package se.squeed.brew18.webflux.lab1;

import static org.junit.jupiter.api.Assertions.fail;

import java.time.Duration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;
import reactor.test.scheduler.VirtualTimeScheduler;

public class Exercise7StepVerifierTest {

    private Exercise7StepVerifier exercise;

    @BeforeEach
    public void setup() {
        exercise = new Exercise7StepVerifier();
    }

    /**
     * Hint StepVerifier.create, expectNext, and verifyComplete
     */
    @Test
    void getFluxCounter() {
        Flux<Integer> flux = exercise.getFluxCounter();

        fail("Implement me");
    }

    /**
     * Test that method getIntervalFlux works as intended. But we do not want to wait 20 seconds...
     *
     * Hint VirtualTimeScheduler.getOrSet() and .expectNoEvent()
     *
     */
    @Test
    void getIntervalFlux() {
        Flux<Long> flux = exercise.getIntervalFlux();

        fail("Implement me");
    }


}
