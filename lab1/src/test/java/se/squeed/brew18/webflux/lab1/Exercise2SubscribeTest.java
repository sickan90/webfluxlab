package se.squeed.brew18.webflux.lab1;


import static org.hamcrest.Matchers.contains;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import se.squeed.brew18.webflux.lab1.core.MySpecialLogger;

class Exercise2SubscribeTest {

    private Exercise2Subscribe exercise;

    MySpecialLogger logger;

    @BeforeEach
    void setup() {
        exercise = new Exercise2Subscribe();
        exercise.logger = spy(exercise.logger);
        logger = exercise.logger;
    }


    @Test
    void toArrayListThroughSubscribe() {
        List<Integer> elements = exercise.toArrayListThroughSubscribe(Flux.just(1, 2, 3, 4));
        MatcherAssert.assertThat(elements, contains(1, 2, 3, 4));
    }

    @Test
    void getErrorMessage() {
        String errorMessage = "print me out please!";
        exercise.printErrorMessage(Flux.create(e -> e.error(new NoSuchMethodError(errorMessage))));
        verify(logger).info(errorMessage);
    }

    @Test
    void printOnComplete() {
        String completeMessage = "completed!";
        exercise.printOnComplete(Flux.just(1, 2), completeMessage);
        verify(logger, times(1)).info(completeMessage);
    }
}