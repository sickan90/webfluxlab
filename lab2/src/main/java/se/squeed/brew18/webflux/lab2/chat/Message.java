package se.squeed.brew18.webflux.lab2.chat;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Document
public class Message {
    @Id
    public UUID id;
    public String text;
    public String who;
    public LocalDateTime created;
}
