package se.squeed.brew18.webflux.lab2.infra;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Mono;
import se.squeed.brew18.webflux.lab2.chat.Message;


@Component
public class MessageMongoConfig implements CommandLineRunner {

    @Autowired
    ReactiveMongoTemplate operations;

    @Override
    public void run(String... args) throws Exception {
        CollectionOptions options = CollectionOptions.empty().capped().size(1024 * 1024).maxDocuments(100L);
        operations.collectionExists(Message.class)
                .flatMap(exists -> exists ? operations.dropCollection(Message.class) : Mono.just(exists))
                .flatMap(ignore -> operations.createCollection(Message.class, options))
                .then()
                .block();
    }
}