import axios from "axios";


const client = axios.create({
    baseURL: '/api',
    headers: {'X-Requested-With': 'XMLHttpRequest'},
});

export default client;
