import React from "react";
import * as ReactDom from "react-dom";

import client from "./Client";
import Message from "./Message";

class Chat extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            messages: []
        };

        this.sendMessage = this.sendMessage.bind(this);
        this.connectToChat = this.connectToChat.bind(this);
        this.addMessage = this.addMessage.bind(this);
    }

    componentDidMount() {
        this.connectToChat();
    }

    sendMessage(event) {
        event.preventDefault();
        var newMessage = ReactDom.findDOMNode(this.refs["message"]).value.trim();
        ReactDom.findDOMNode(this.refs["message"]).value = "";
        if (newMessage && newMessage !== "") {
            client.post("/messages", {
                who: this.props.username,
                text: newMessage
            }).then(m => this.addMessage(m.data))
        }
    }

    connectToChat() {
        var source = new EventSource("/api/messages/sse");
        var addMessageFunc = this.addMessage;
        source.onmessage = function(event) {
            addMessageFunc(JSON.parse(event.data));
        };
    }

    addMessage(newMessage) {
        if (! this.state.messages.find(m => m.id === newMessage.id)) {
            this.setState({messages: this.state.messages.concat([newMessage])});
        }
    }


    render() {
        return (
            <div id="chat">
                <div id="messages">
                    {this.state.messages.map(message =>
                        <Message key={message.id} message={message}/>
                    )}
                </div>
                <div id="newMessage">
                    <input type="text" className="form-control" placeholder="Message" id="message" ref="message"/>
                    <button type="button" className="btn btn-primary" onClick={this.sendMessage}>Send</button>
                </div>
            </div>
        )
    }
}

export default Chat;
