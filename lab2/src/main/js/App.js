'use strict';

import React from "react";
import * as ReactDom from "react-dom";
import "../scss/custom.scss";

import Chat from "./Chat"

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: null
        };

        this.setUsername = this.setUsername.bind(this);
    }

    setUsername(event) {
        event.preventDefault();
        var username = ReactDom.findDOMNode(this.refs["username"]).value.trim();
        this.setState({username: username})
    }

    render() {
        if (this.state.username) {
            return <Chat username={this.state.username}/>
        } else {
            return <div className="form-group">
                <input type="text" className="form-control" placeholder="Username" id="username" ref="username"/>
                <button type="button" className="btn btn-primary" onClick={this.setUsername}>Enter</button>
            </div>
        }

    }
}


ReactDom.render(
    <App/>,
    document.getElementById('react')
);
