import React from "react";


class Message extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <p>
                <b>{this.props.message.who}:</b> {this.props.message.text}
            </p>
        )
    }
}

export default Message;
