# Lab 2 - creating a chat
In this lab we will be creating a small chat application.

## Creating the repo
This repo is composed from the follow items when starting a new project from spring initializer:
* Reactive Web (which brings webflux)
* Reactive MongoDB
* Embedded MongoDB


## Let's get the started
The first thing we will wanna do is to remove the 'test' scope from the embedded mongoDB in the _pom.xml_ so we dont need an external database when we are running our application.

### Our data object


The data object that we will be using is a very simple document which we will use to store our data in our mongo database.

```java
@Document
public class Message {
    @Id
    public UUID id;
    public String text;
    public String who;
    public LocalDateTime created;
}
```
The *Message* class is a simple POJO that contains Id, text, who wrote it, and when it was created. The @Document annotation markes the class as a MongoDB documents. These documents will be mapped to collections in MongoDB by Spring Data.

### Our repository
```java
public interface MessageRepository extends ReactiveMongoRepository<Message, UUID> {
    @Tailable
    Flux<Message> findBy();
}
```
The MessageRepository extends the Spring Data interface ReactiveMongoRepository. What this mean is that Spring Data will generate the code which implements that interface, so we do not have to provide an implementation. Methods that we will be getting for free are for example:
* Mono\<Message\> save()
* Mono\<Void\> delete()
* Mono\<Message\> findById()
* Flux\<Message\> findAll()

Mono\<Void\> is a lite special. What this mean is that it will not return an object, but it will be able to notify when the operation is done. 


### Our service
````java
@Service
public class MessageService {

    private MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public Flux<Message> getAllMessages() {
        return messageRepository.findAll();
    }

    public Mono<Message> addMessage(Message message) {
        message.created = LocalDateTime.now();
        message.id = UUID.randomUUID();
        return messageRepository.save(message);
    }

    public Flux<Message> getAllMessagesSSE() {
        return messageRepository.findBy();
    }
}

````
There is not really anything special about the service. The only real difference to MVC how you would normally write a service is that we are return Mono and Flux objects instead of the object directly or in List/Array.

### Our router
````java
@Configuration
public class MessageRouter {

    @Autowired
    private MessageService messageService;

    @Bean
    RouterFunction<ServerResponse> messageRoutes() {
        return
                route(
                        RequestPredicates.GET("/api/messages"),
                        req -> ok().body(messageService.getAllMessages(), Message.class)
                ).and(route(
                        RequestPredicates.POST("/api/messages"),
                        req -> req
                                .body(toMono(Message.class))
                                .flatMap(m -> ok().body(messageService.addMessage(m), Message.class))
                )).and(route(
                        RequestPredicates.GET("/api/messages/sse"),
                        req -> ok().contentType(MediaType.TEXT_EVENT_STREAM)
                                .body(messageService.getAllMessagesSSE(), Message.class)
                ));
    }
}
````
Here is something that is a bit different. With WebFlux we got the ability to define our endpoint using a functional approach. WebFlux works great with ordinary @Controller and @RestController, but we are following the new approach here.

## Let's test the application
Spring MVC got the MockMVC which can be used to test the rest endpoints. With spring webflux we also have a a new test tool, WebTestClient. It also has a fluent language which makes it quite nice to work with.
````java
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureWebTestClient
class MessageTest {

    @Autowired
    private WebTestClient webTestClient;

    private final String MESSAGES_URI = "/api/messages";

    @Test
    public void testMessages() {
        webTestClient.get()
                .uri(MESSAGES_URI)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$").isEmpty();
    }

    @Test
    @DirtiesContext
    public void addMessage() {
        Message message = new Message();
        message.text = "Hello World!";
        message.who = "Test";

        webTestClient.post()
                .uri(MESSAGES_URI)
                .body(Mono.just(message), Message.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.who").isEqualTo("Test")
                .jsonPath("$.text").isEqualTo("Hello World!")
                .jsonPath("$.id").isNotEmpty()
                .jsonPath("$.created").isNotEmpty();

        webTestClient.get()
                .uri(MESSAGES_URI)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$[0].who").isEqualTo("Test")
                .jsonPath("$[0].text").isEqualTo("Hello World!")
                .jsonPath("$[0].id").isNotEmpty()
                .jsonPath("$[0].created").isNotEmpty();

    }

    @Test
    @DirtiesContext
    public void testSSE() {
        Message message = new Message();
        message.text = "Hello World!";
        message.who = "Test";

        webTestClient.post()
                .uri(MESSAGES_URI)
                .body(Mono.just(message), Message.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.who").isEqualTo("Test")
                .jsonPath("$.text").isEqualTo("Hello World!")
                .jsonPath("$.id").isNotEmpty()
                .jsonPath("$.created").isNotEmpty();

        StepVerifier.create(
                webTestClient.get()
                        .uri(MESSAGES_URI + "/sse")
                        .accept(MediaType.TEXT_EVENT_STREAM)
                        .exchange()
                        .expectStatus().isOk()
                        .returnResult(Message.class)
                        .getResponseBody())
                .expectNextMatches(m -> m.text.equals(message.text) && m.who.equals(message.who))
                .thenCancel()
                .verify();

    }
}
````

















